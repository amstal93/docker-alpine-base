FROM alpine:3
LABEL image.authors="christian@dotslashme.com"
RUN apk update \
		&& apk upgrade \
		&& apk add shadow su-exec \
		&& rm -rf /var/cache/apk/* \
		&& groupadd -o -g 1000 dockrun \
		&& useradd -o -u 1000 -g 1000 -M -s /bin/sh dockrun

COPY docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]
